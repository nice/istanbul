# Intervention en master de cartographie - projet Bosphore

Cet espace comprend :
- [Une version pdf de la présentation présentée lors de l'intervention](https://framagit.org/nice/istanbul/-/blob/main/Cartographier_les_flux_et_les_r%C3%A9seaux_-_istanbul_2022.pdf)
- Une [liste de ressources (liens et références)](https://framagit.org/nice/istanbul/-/blob/main/Liens.md)
- Un [dossier data](https://framagit.org/nice/istanbul/-/tree/main/data) qui contient des extractions de données issues de Comtrade, Comwaste et Ressourcetrade.earth à partir de l'exemple du commerce international de ferraille en 2014

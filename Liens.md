# Liste des liens mentionnés lors de l'intervention

- NETMAP : [l'exemple du commerce international de ferraille](http://www.geotests.net/test/net_map/fer_mep.html)
- La base de données UN COMTRADE : [https://comtrade.un.org/data/](https://comtrade.un.org/data/)
- Le prototype COMWASTE : [l'exemple du commerce international de ferraille](http://www.geotests.net/comwaste/index.php?r=%27All%27&p=%27All%27&c=%277204%20-%20Ferrous%20waste%20and%20scrap;%20remelting%20scrap%20ingots%20of%20iron%20or%20steel%27&ymin=2014&u=%27w%27&tf=%27All%27&t=%27All%27&fmin=0&fmax=0)
- La plateforme ressourcetrade.earth : [l'exemple du commerce international de ferraille](https://resourcetrade.earth/?year=2014&category=154&units=value&autozoom=1)
- [Les ateliers Mondis-Géovizu](https://mondis.hypotheses.org/410)
- La bibliothèque de fonctions javascript [D3.js](https://d3js.org/)

# Références :

- Joliveau, T. (2011). 500 millions d'amis, la carte de Facebook. [https://mondegeonumerique.wordpress.com/2011/01/10/500-millions-damis-la-carte-de-facebook-1-deconstruction/](https://mondegeonumerique.wordpress.com/2011/01/10/500-millions-damis-la-carte-de-facebook-1-deconstruction/)
- Baron, M., Eckert, D. & Jégou, L. (2011). Peut-on démêler l’écheveau mondial des collaborations scientifiques? M@ppemonde, 2(102). [http://mappemonde-archive.mgm.fr/num30/internet/int11201.html](http://mappemonde-archive.mgm.fr/num30/internet/int11201.html)
- Jégou, L., & Maisonobe, M. (2013). Les collaborations scientifiques mondiales, 1999-2008, une application web de géovisualisation. M@ppemonde, 4(112). [http://mappemonde.mgm.fr/num40/fig13/fig13403.html](http://mappemonde-archive.mgm.fr/num40/fig13/fig13403.html)
- Maisonobe, M. (2017). « Allo la Terre ? » Visualiser la circulation des ressources naturelles dans le monde avec resourcetrade.earth. M@ppemonde, 121. [http://mappemonde.mgm.fr/121_int1/](http://mappemonde.mgm.fr/121_int1/)

# Tutoriels et ressources complémentaires :
- Supports de la formation à la visualisation interactive de réseaux - intervention préparée avec Laurent Jégou pour la semaine DATASHS 2019 :
    - [Introduction à l’exploration et la représentation de flux à l’échelle mondiale : l’exemple du commerce international et des collaborations scientifiques entre villes](https://framagit.org/MarionMai/data-shs)
- [Utiliser le plugin Network Analysis avec QGIS3](https://framagit.org/MarionMai/using-network_analysis-on-qgis-3)
- [Analyse de réseaux avec R - séance Visualisation - école d'été de Nice 2019](https://framagit.org/MarionMai/Visualisation-de-reseaux)
- [Carnet de recherche du groupe fmr](https://groupefmr.hypotheses.org/)
